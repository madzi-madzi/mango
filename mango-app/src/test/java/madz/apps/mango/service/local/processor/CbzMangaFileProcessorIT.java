package madz.apps.mango.service.local.processor;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import madzi.apps.mango.model.Manga;
import madzi.apps.mango.service.local.processor.CbzMangaFileProcessor;

public class CbzMangaFileProcessorIT {

    private CbzMangaFileProcessor processor = new CbzMangaFileProcessor();

    @Test
    public void testReadMng1() throws URISyntaxException {
        Path path = Paths.get("src/test/resources/mng1.cbz");
        Manga manga = processor.process(path.toFile());
        Assertions.assertNotNull(manga);
        Assertions.assertEquals("mng1", manga.getTitle());
    }

    @Test
    public void testReadMng2() throws URISyntaxException {
        Path path = Paths.get("src/test/resources/mng2.cbz");
        Manga manga = processor.process(path.toFile());
        Assertions.assertNotNull(manga);
        Assertions.assertEquals("mng2", manga.getTitle());
    }

    @Test
    public void testReadMng3() throws URISyntaxException {
        Path path = Paths.get("src/test/resources/mng3.cbz");
        Manga manga = processor.process(path.toFile());
        Assertions.assertNotNull(manga);
        Assertions.assertEquals("mng3", manga.getTitle());
    }

    @Test
    public void testReadMng4() throws URISyntaxException {
        Path path = Paths.get("src/test/resources/mng4.cbz");
        Manga manga = processor.process(path.toFile());
        Assertions.assertNotNull(manga);
        Assertions.assertEquals("mng4", manga.getTitle());
    }
}
