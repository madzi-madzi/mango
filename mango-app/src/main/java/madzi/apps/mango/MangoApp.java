package madzi.apps.mango;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import madzi.apps.mango.controller.FxmlView;
import madzi.apps.mango.i18n.Message;

public class MangoApp extends Application {

    private static final Logger logger = LoggerFactory.getLogger(MangoApp.class);

    private GenericApplicationContext context;

    @Override
    public void init() {
        logger.info("Start initialization...");
        context = new AnnotationConfigApplicationContext("madzi.apps.mango");
    }

    @Override
    public void stop() {
        logger.info("Start finalization...");
        context.close();
    }

    @Override
    public void start(final Stage stage) throws Exception {
        FxmlView libraryView = context.getBean("libraryView", FxmlView.class);
        ResourceBundle bundle = context.getBean(ResourceBundle.class);
        stage.setScene(new Scene(libraryView.getView(), 800.0, 600.0));
        stage.setTitle(bundle.getString(Message.APP_TITLE.key()));

        stage.show();
    }

    public static void main(final String... args) {
        launch(args);
    }
}
