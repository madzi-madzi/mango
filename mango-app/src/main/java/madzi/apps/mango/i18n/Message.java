package madzi.apps.mango.i18n;

public enum Message {
    APP_TITLE("app.title");

    private Message(final String key) {
        this.key = key;
    }

    private final String key;

    public String key() {
        return key;
    }
}
