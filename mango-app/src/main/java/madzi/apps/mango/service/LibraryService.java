package madzi.apps.mango.service;

import java.util.Collection;

import madzi.apps.mango.model.Manga;

public interface LibraryService {

    Collection<Manga> findAll();
}
