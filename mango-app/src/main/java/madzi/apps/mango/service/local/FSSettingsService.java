package madzi.apps.mango.service.local;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import madzi.apps.mango.service.SettingsService;

@Service
public class FSSettingsService implements SettingsService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(FSSettingsService.class);

    @Value("${user.home}")
    private String homeFolder;

    @Value("${library.folder}")
    private String libraryFolder;
    private Path libraryPath;

    @Override
    public void afterPropertiesSet() throws Exception {
        loadProgramConfig();

        libraryPath = fixHomeFolder(libraryFolder);
    }

    @Override
    public Path libraryPath() {
        return libraryPath;
    }

    private void loadProgramConfig() {
        Path configPath = Paths.get(homeFolder, ".mango", "mango.properties");
        if (configPath.toFile().exists()) {
            logger.info("Found user settings at '{}'.", configPath);
            Properties props = new Properties();
            try {
                props.load(Files.newInputStream(configPath, StandardOpenOption.READ));

                libraryFolder = props.getProperty("library.folder", libraryFolder).trim();
            } catch (IOException ioException) {
                logger.error("Unable to load program configuration file. (Will be used default settings)");
            }
        }
    }

    private Path fixHomeFolder(final String folder) {
        Objects.requireNonNull(folder, "Folder " + folder + " should not be NULL");
        if (folder.startsWith("~/")) {
            return Paths.get(homeFolder, folder.substring(2));
        }

        return Paths.get(folder);
    }
}
