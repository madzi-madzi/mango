package madzi.apps.mango.service.local.processor;

import java.io.File;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import madzi.apps.mango.model.Manga;
import madzi.apps.mango.model.fx.FxManga;

@Component
public class CbzMangaFileProcessor implements MangaFileProcessor {

    private static final Logger logger = LoggerFactory.getLogger(CbzMangaFileProcessor.class);

    @Override
    public boolean acceptable(final File file) {
        return file.getName().toLowerCase().endsWith(".cbz");
    }

    @Override
    public Manga process(final File file) {
        try (ZipFile zipFile = new ZipFile(file)) {
            ZipEntry zipEntry = zipFile.getEntry(".metadata.xml");
            if (zipEntry != null) { // we have full meta here
                // TODO: get meta
            }

            // search metadata into comment
            String comment = zipFile.getComment();
            if (comment != null) {
                // TODO: extract meta from comment
            }

            // TODO: process archive
            var fileName = file.getName();
            zipFile.getEntry("cover.jpg");

            return new FxManga(file.toPath(), fileName.substring(0, fileName.length() - 4));
        } catch (Exception exception) {
            logger.error("Unable to process file {}", file, exception);
        }

        return null;
    }
}
