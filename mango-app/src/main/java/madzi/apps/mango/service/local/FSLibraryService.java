package madzi.apps.mango.service.local;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import madzi.apps.mango.model.Manga;
import madzi.apps.mango.service.LibraryService;
import madzi.apps.mango.service.SettingsService;
import madzi.apps.mango.service.local.processor.MangaFileProcessor;

@Service
public class FSLibraryService implements LibraryService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(FSLibraryService.class);

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private Collection<MangaFileProcessor> processors;

    private final Set<Manga> mangas = new HashSet<>();

    @Override
    public Collection<Manga> findAll() {
        return mangas;
    }

    @Override
    public void afterPropertiesSet() {
        rebuild();
    }

    private void rebuild() {
        mangas.clear();
        Path libraryPath = settingsService.libraryPath();

        logger.info("Rebuild library into location: {}", libraryPath);
        try {
            Files.walkFileTree(libraryPath, new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws IOException {
                    var file = filePath.toFile();
                    logger.info("Found: {}", file);
                    if (file.isFile() && file.canRead()) {
                        logger.info("File looks good");
                        processors.forEach(processor -> {
                            if (processor.acceptable(filePath.toFile())) {
                                logger.info("File is suitable");
                                var manga = processor.process(filePath.toFile());
                                if (manga != null) {
                                    mangas.add(manga);
                                }
                            }
                        });
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path filePath, IOException exc) throws IOException {
                    logger.warn("Unable to visit file: {}", filePath);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ioException) {
            logger.error("Unable to build library", ioException);
        }
    }
}
