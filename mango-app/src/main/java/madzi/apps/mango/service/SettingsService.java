package madzi.apps.mango.service;

import java.nio.file.Path;

public interface SettingsService {

    Path libraryPath();
}
