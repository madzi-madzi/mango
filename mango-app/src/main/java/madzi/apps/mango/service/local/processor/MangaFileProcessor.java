package madzi.apps.mango.service.local.processor;

import java.io.File;

import madzi.apps.mango.model.Manga;

public interface MangaFileProcessor {

    boolean acceptable(File file);

    Manga process(File file);
}
