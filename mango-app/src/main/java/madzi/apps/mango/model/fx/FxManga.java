package madzi.apps.mango.model.fx;

import java.nio.file.Path;

import madzi.apps.mango.model.Manga;

public class FxManga implements Manga {

    private final Path path;
    private final String title;

    public FxManga(final Path path, final String title) {
        this.path = path;
        this.title = title;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSummray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getYear() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getWriter() {
        // TODO Auto-generated method stub
        return null;
    }
}
