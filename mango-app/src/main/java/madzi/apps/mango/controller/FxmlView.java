package madzi.apps.mango.controller;

import javafx.scene.Parent;

public final class FxmlView {

    private final FxmlController controller;
    private final Parent view;

    public FxmlView(final FxmlController controller, final Parent view) {
        this.controller = controller;
        this.view = view;
    }

    public FxmlController getController() {
        return controller;
    }

    public Parent getView() {
        return view;
    }
}
