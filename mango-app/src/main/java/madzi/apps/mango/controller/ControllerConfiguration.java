package madzi.apps.mango.controller;

import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

@Configuration
public class ControllerConfiguration {

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;
    @Autowired
    private ResourceBundle resourceBundle;

    @Bean
    public FxmlView libraryView() {
        return createFxmlView("/fxml/library.fxml");
    }

    @Bean
    @Scope("prototype")
    public FxmlView mangaView() {
        return createFxmlView("/fxml/manga.fxml");
    }

    private FxmlView createFxmlView(final String location) {
        try {
            var fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(ControllerConfiguration.class.getResource(location));
            fxmlLoader.setResources(resourceBundle);
            Parent parent = fxmlLoader.load();
            FxmlController controller = fxmlLoader.getController();
            autowireCapableBeanFactory.autowireBean(controller);
            controller.afterPropertiesSet();

            return new FxmlView(controller, parent);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}
