package madzi.apps.mango.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import madzi.apps.mango.model.Manga;

public class MangaController implements FxmlController {

    private static final Logger logger = LoggerFactory.getLogger(MangaController.class);

    private Manga manga;

    public Manga getManga() {
        return manga;
    }

    public void setManga(final Manga manga) {
        this.manga = manga;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Initializing Manga Controller");
    }
}
