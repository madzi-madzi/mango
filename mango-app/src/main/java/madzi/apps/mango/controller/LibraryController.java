package madzi.apps.mango.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import madzi.apps.mango.model.Manga;
import madzi.apps.mango.service.LibraryService;

public class LibraryController implements FxmlController {

    private static final Logger logger = LoggerFactory.getLogger(LibraryController.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    private LibraryService libraryService;

    @FXML
    private TableView<Manga> tableView;
    @FXML
    private TableColumn<Manga, String> tcTitle;

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Initialize Library Controller");
        tcTitle.setCellValueFactory(new PropertyValueFactory<Manga, String>("title"));
        tableView.getItems().addAll(libraryService.findAll());
    }

    @FXML
    public void onRowClick(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
            Manga manga = tableView.getSelectionModel().getSelectedItem();
            if (manga != null) {
                logger.info("Choosed manga: {}", manga);
                FxmlView mangaView = context.getBean("mangaView", FxmlView.class);
                MangaController mangaController = (MangaController) mangaView.getController();
                mangaController.setManga(manga);
                Stage stage = new Stage();
                stage.setTitle(manga.getTitle());
                stage.setScene(new Scene(mangaView.getView(), 800.0, 600.0));

                stage.show();
            }
        }
    }

    @FXML
    public void onExitAction() {
        Platform.exit();
    }
}
