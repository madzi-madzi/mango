package madzi.apps.mango;

import java.util.ResourceBundle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application.properties")
public class MangoConfiguration {

    @Bean
    public ResourceBundle resourceBundle() {
        return ResourceBundle.getBundle("mango");
    }
}
