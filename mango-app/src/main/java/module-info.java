module madzi.apps.mango {
    requires org.slf4j;
    requires org.slf4j.simple;

    requires javafx.graphics;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;

    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires spring.messaging;

    opens madzi.apps.mango to spring.core;
    opens madzi.apps.mango.model.fx to javafx.base;
    opens madzi.apps.mango.service.local to spring.core, spring.beans, spring.context;
    opens madzi.apps.mango.service.local.processor to spring.core, spring.beans, spring.context;
    opens madzi.apps.mango.controller to spring.core, spring.beans, spring.context, javafx.fxml;

    exports madzi.apps.mango;
}
