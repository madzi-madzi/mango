<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>madzi.apps</groupId>
        <artifactId>mango-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../mango-parent</relativePath>
    </parent>

    <artifactId>mango-app</artifactId>
    <packaging>jar</packaging>

    <name>MANga GO::Application</name>
    <description>Manga desktop reader</description>

    <properties>
        <modules.dir>${project.build.directory}/modules</modules.dir>
        <module.name>madzi.apps.mango</module.name>
        <main.class>madzi.apps.mango.MangoApp</main.class>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
        </dependency>

        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-graphics</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-fxml</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-messaging</artifactId>
            <version>${spring.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <release>${java.version}</release>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${modules.dir}</outputDirectory>
                            <includeScope>runtime</includeScope>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <outputDirectory>${modules.dir}</outputDirectory>
                </configuration>
            </plugin>
                <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <encoding>${encoding}</encoding>
                            <outputDirectory>${modules.dir}/${module.name}</outputDirectory>
                            <resources>
                                <resource>
                                    <filtering>true</filtering>
                                    <directory>${basedir}/src/main/resources</directory>
                                    <include>**/*</include>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.moditect</groupId>
                <artifactId>moditect-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>add-module-info</id>
                        <phase>package</phase>
                        <goals>
                            <goal>add-module-info</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${modules.dir}</outputDirectory>
                            <overwriteExistingFiles>true</overwriteExistingFiles>
                            <modules>
                                <module>
                                    <artifact>
                                        <groupId>org.springframework</groupId>
                                        <artifactId>spring-core</artifactId>
                                        <version>${spring.version}</version>
                                    </artifact>
                                    <moduleInfoSource>
                                        module spring.core {
                                            exports org.springframework.core;
                                        }
                                    </moduleInfoSource>
                                </module>
                                <module>
                                    <artifact>
                                        <groupId>org.springframework</groupId>
                                        <artifactId>spring-context</artifactId>
                                        <version>${spring.version}</version>
                                    </artifact>
                                    <moduleInfoSource>
                                        module spring.context {
                                            exports org.springframework.cache;
                                            exports org.springframework.cache.annotation;
                                            exports org.springframework.cache.concurrent;
                                            exports org.springframework.cache.config;
                                            exports org.springframework.cache.interceptor;
                                            exports org.springframework.cache.support;
                                            exports org.springframework.context;
                                            exports org.springframework.context.annotation;
                                            exports org.springframework.context.config;
                                            exports org.springframework.context.event;
                                            exports org.springframework.context.expression;
                                            exports org.springframework.context.i18n;
                                            exports org.springframework.context.index;
                                            exports org.springframework.context.support;
                                            exports org.springframework.context.weaving;
                                            exports org.springframework.ejb.access;
                                            exports org.springframework.ejb.config;
                                            exports org.springframework.format;
                                            exports org.springframework.format.annotation;
                                            exports org.springframework.format.datetime;
                                            exports org.springframework.format.datetime.joda;
                                            exports org.springframework.format.datetime.standard;
                                            exports org.springframework.format.number;
                                            exports org.springframework.format.number.money;
                                            exports org.springframework.format.support;
                                            exports org.springframework.instrument.classloading;
                                            exports org.springframework.instrument.classloading.glassfish;
                                            exports org.springframework.instrument.classloading.jboss;
                                            exports org.springframework.instrument.classloading.tomcat;
                                            exports org.springframework.instrument.classloading.weblogic;
                                            exports org.springframework.instrument.classloading.websphere;
                                            exports org.springframework.jmx;
                                            exports org.springframework.jmx.access;
                                            exports org.springframework.jmx.export;
                                            exports org.springframework.jmx.export.annotation;
                                            exports org.springframework.jmx.export.assembler;
                                            exports org.springframework.jmx.export.metadata;
                                            exports org.springframework.jmx.export.naming;
                                            exports org.springframework.jmx.export.notification;
                                            exports org.springframework.jmx.support;
                                            exports org.springframework.jndi;
                                            exports org.springframework.jndi.support;
                                            exports org.springframework.remoting;
                                            exports org.springframework.remoting.rmi;
                                            exports org.springframework.remoting.soap;
                                            exports org.springframework.remoting.support;
                                            exports org.springframework.scheduling;
                                            exports org.springframework.scheduling.annotation;
                                            exports org.springframework.scheduling.concurrent;
                                            exports org.springframework.scheduling.config;
                                            exports org.springframework.scheduling.support;
                                            exports org.springframework.scripting;
                                            exports org.springframework.scripting.bsh;
                                            exports org.springframework.scripting.config;
                                            exports org.springframework.scripting.groovy;
                                            exports org.springframework.scripting.support;
                                            exports org.springframework.stereotype;
                                            exports org.springframework.ui;
                                            exports org.springframework.ui.context;
                                            exports org.springframework.ui.context.support;
                                            exports org.springframework.validation;
                                            exports org.springframework.validation.annotation;
                                            exports org.springframework.validation.beanvalidation;
                                            exports org.springframework.validation.support;
                                        }
                                    </moduleInfoSource>
                                </module>
                                <module>
                                    <artifact>
                                        <groupId>org.springframework</groupId>
                                        <artifactId>spring-beans</artifactId>
                                        <version>${spring.version}</version>
                                    </artifact>
                                    <moduleInfoSource>
                                        module spring.beans {
                                            exports org.springframework.beans;
                                            exports org.springframework.beans.annotation;
                                            exports org.springframework.beans.factory;
                                            exports org.springframework.beans.factory.annotation;
                                            exports org.springframework.beans.factory.config;
                                            exports org.springframework.beans.factory.groovy;
                                            exports org.springframework.beans.factory.parsing;
                                            exports org.springframework.beans.factory.serviceloader;
                                            exports org.springframework.beans.factory.support;
                                            exports org.springframework.beans.factory.wiring;
                                            exports org.springframework.beans.factory.xml;
                                            exports org.springframework.beans.propertyeditors;
                                            exports org.springframework.beans.support;
                                        }
                                    </moduleInfoSource>
                                </module>
                                <module>
                                    <artifact>
                                        <groupId>org.springframework</groupId>
                                        <artifactId>spring-messaging</artifactId>
                                        <version>${spring.version}</version>
                                    </artifact>
                                    <moduleInfoSource>
                                        module spring.messaging {
                                            exports org.springframework.messaging;
                                        }
                                    </moduleInfoSource>
                                </module>
                            </modules>
                        </configuration>
                    </execution>
                    <execution>
                        <id>create-runtime-image</id>
                        <phase>package</phase>
                        <goals>
                            <goal>create-runtime-image</goal>
                        </goals>
                        <configuration>
                            <modulePath>
                                <path>${modules.dir}</path>
                            </modulePath>
                            <modules>
                                <module>${module.name}</module>
                            </modules>
                            <launcher>
                                <name>${main.class}</name>
                                <module>${module.name}/${main.class}</module>
                            </launcher>
                            <compression>2</compression>
                            <stripDebug>true</stripDebug>
                            <outputDirectory>${project.build.directory}/jlink-image</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>io.gameta.allure</groupId>
                <artifactId>allure-maven</artifactId>
                <version>2.9</version>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <executable>java</executable>
                    <arguments>
                        <argument>--module-path</argument>
                        <argument>${modules.dir}</argument>
                        <argument>--module</argument>
                        <argument>${module.name}/${main.class}</argument>
                    </arguments>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
